import random
from calculator import getnodes,run,getlift,changeNodes
from typing import List, Optional, Callable, Tuple
import math
Genome = []
Population = []




def generate_genome(length):
	Genome=[]
	for i in range(length):
		gene = format(random.uniform(0.01,0.1),'.16f')
		gene = round(float(gene),2)
		gene = format(gene,'.16f')
		Genome.append(str(gene))
	return Genome

def generate_population(size,length):
	for i in range(size):
		Population.append(generate_genome(length))
	return Population

def fitness(Genome,i):
	print(i)
	if float(Genome[0]) > 0.2 or float(Genome[1]) > 0.2 or float(Genome[2]) > 0.2 or float(Genome[3]) > 0.2:
		return 0
	node1,node2,node3,node4,nodelines=getnodes()
	changeNodes(node1,node2,node3,node4,nodelines,format(float(Genome[0]),'.16f'),format(float(Genome[1]),'.16f'),format(float(Genome[2]),'.16f'),format(float(Genome[3]),'.16f'))
	run()
	return getlift()

def selection_pair(Population):
	return random.choices(
		population = Population,
		weights = [fitness(Genome,1) for Genome in Population],
		k = 3
		)
		
def single_point_crossover(GenomeA,GenomeB,GenomeC):
	buffera = 0
	bufferb = 0
	GenomeAFINAL = []
	GenomeBFINAL = []
	for i in range(4):
		buffera = float(GenomeA[i])+float(GenomeB[i])
		buffera = buffera/2
		GenomeAFINAL.append(round(buffera,2))
	for i in range(4):
		bufferb = float(GenomeB[i]) + float(GenomeC[i])
		bufferb = bufferb/2
		GenomeBFINAL.append(round(bufferb,2))

	return GenomeAFINAL,GenomeBFINAL
def mutation(Genome):
	probability = 1.7
	for _ in range(1):
		index = random.randrange(len(Genome))
		if random.uniform(0,2) > probability:
			Genome[index] = format(float(Genome[index]),'.16f')
		else:
			Genome[index] = format(float(Genome[index])*1.1,'.16f')
	for i in range(len(Genome)):
		Genome[i] = round(float(Genome[i]),2)
		Genome[i] = str(Genome[i])
	return Genome




def run_evolution(generation_limit):

	population = generate_population(10,4)

	for i in range(generation_limit):


		population = sorted(population, key=lambda Genome:fitness(Genome,i), reverse=True)
	

		next_generation = population[0:3]
		print("population finished")
		bestgen = open("C:/Users/skyma/Desktop/tahseen/geneticlift/record.txt", "a")
		node1,node2,node3,node4,nodelines = getnodes()
		changeNodes(node1,node2,node3,node4,nodelines,next_generation[0][0],next_generation[0][1],next_generation[0][2],next_generation[0][3])
		run()
		lift = getlift()
		text = f"parent number 1: generation number = {i} lift = {lift} node1 = {next_generation[0][0]} node2 = {next_generation[0][1]} node3 = {next_generation[0][2]} node4 = {next_generation[0][3]}"
		node1,node2,node3,node4,nodelines = getnodes()
		changeNodes(node1,node2,node3,node4,nodelines,next_generation[1][0],next_generation[1][1],next_generation[1][2],next_generation[1][3])
		run()
		lift = getlift()
		text2 = f"parent number 2: generation number = {i} lift = {lift} node1 = {next_generation[1][0]} node2 = {next_generation[1][1]} node3 = {next_generation[1][2]} node4 = {next_generation[1][3]}"
		bestgen.write(text)
		bestgen.write('\n')
		bestgen.write(text2)
		node1,node2,node3,node4,nodelines = getnodes()
		changeNodes(node1,node2,node3,node4,nodelines,next_generation[2][0],next_generation[2][1],next_generation[2][2],next_generation[2][3])
		run()
		lift = getlift()
		text3 = f"parent number 3: generation number = {i} lift = {lift} node1 = {next_generation[2][0]} node2 = {next_generation[2][1]} node3 = {next_generation[2][2]} node4 = {next_generation[2][3]}"
		bestgen.write('\n')
		bestgen.write('\n')
		bestgen.close()
		for j in range(int(len(population) / 2) - 1):
			parents = selection_pair(population)
			offspring_a, offspring_b = single_point_crossover(parents[0], parents[1],parents[2])
			offspring_a = mutation(offspring_a)
			offspring_b = mutation(offspring_b)
			next_generation += [offspring_a, offspring_b]

		population = next_generation

	return i,getlift(),getnodes()[0],getnodes()[1],getnodes()[2],getnodes()[3]



print(run_evolution(10))